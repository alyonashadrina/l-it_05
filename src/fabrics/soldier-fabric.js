import {Soldier} from "../core/models/soldier";

let instanse = null;

export class SoldierFabric {
    static getInstance() {
        return new SoldierFabric();
    }


    constructor(){
        if(instanse){
            return instanse;
        }
        instanse = this;
    }


    createSoldier(data) {
      return new Soldier( data.health, data.recharge )
    }

    createSoldiers(arr) {
        return arr.map( (data) => this.createSoldier(data) )
    }
}
