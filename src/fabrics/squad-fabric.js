import {Squad} from "../core/models/squad";
import {SoldierFabric} from "./soldier-fabric";
import {VehicleFabric} from "./vehicles-fabric";

let instanse = null;

export class SquadFabric {
    static getInstance() {
        return new SquadFabric();
    }

    constructor(){
        if(instanse){
            return instanse;
        }
        instanse = this;
    }

    createSquad(units) {
      if (units.type == 'soldiers') {
        let squadGenerator = SoldierFabric.getInstance()
        let squadUnits = squadGenerator.createSoldiers(units.units)
        console.log(`-- Squad of ${units.units.length} soldiers`)
        return new Squad(squadUnits, units.type)
      } else if (units.type == 'vehicles'){
        let squadGenerator = VehicleFabric.getInstance()
        let squadUnits = squadGenerator.createVehicles(units.units)
        console.log(`-- Squad of ${units.units.length} vehicles`)
        return new Squad(squadUnits, units.type)
      }
    }

    createSquads(arr) {
        console.log(`${arr.length} squads will be created`)
        return arr.map( (data) => this.createSquad(data) )
    }
}
