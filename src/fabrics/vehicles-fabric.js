import {Vehicle} from "../core/models/vehicles";

let instanse = null;

export class VehicleFabric {
    static getInstance() {
        return new VehicleFabric();
    }


    constructor(){
        if(instanse){
            return instanse;
        }
        instanse = this;
    }


    createVehicle(data) {
      let veh = new Vehicle ( data.health, data.recharge, data.operators );
      return veh
    }

    createVehicles(arr) {
      return arr.map( (data) => this.createVehicle(data) )
    }
}
