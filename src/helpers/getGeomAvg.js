export function getGeomAvg(data) {
  let total = 0;
  data.map( (val) => (total += val) )
  return total / data.length;
}
