import {BaseUnit} from "./base-unit";

export class Unit extends BaseUnit {

    constructor(health, recharge) {
        super()
        this.health = health;
        this.recharge = recharge;
        this.startRechargeTime = 0;
    }

    makeDamage() {
        throw new Error('makeDamage() from Unit need to be defined');
    }

    attackSuccess() {
        throw new Error('attackSuccess() from Unit need to be defined');
    }

    damageRecieve(damage) {

    }

    isAlive() {
      return this.health > 0 ? true : false
    }

    setRecharge(val) {
        if (val < 100) {
            this.recharge = 100;
        } else if (val > 2000) {
            this.recharge = 2000;
        } else {
            this.recharge = val;
        }
    }

    get isRecharging() {
      return this.startRechargeTime > Date.now() ? true : false
    }



    startRecharge() {
      this.startRechargeTime = Date.now() + this.recharge
    }

    getHealth() {

    }

    setHealth(val) {
        if (val < 0) {
            this.health = 0;
        } else if (val > 100) {
            this.health = 100;
        } else {
            this.health = val;
        }
    }

}
