import {Unit} from "./unit";
import {getRandom} from "../../helpers/getRandom"

export class Soldier extends Unit {
    _experience = 0;

    makeDamage() {
      this.startRecharge();
      return ( 0.05 + this._experience / 100 )
    }

    attackSuccess() {
        let success = 0.5 * (1 + this.health / 100) * getRandom(50 + this._experience, 100) / 100;
        return success
    }

    set experience(val) {
        if (val < 0) {
            this._experience = 0;
        } else if (val > 50) {
            this._experience = 50;
        } else {
            this._experience = val;
        }
    }

    get experience() {
        return this._experience
    }

}
