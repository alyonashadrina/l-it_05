import {getRandom} from "../../helpers/getRandom";

export class Strategy {
    constructor(target) {
        // strategy:
        this.targetArmy = target;
    }
    getRandomSquad() {
      return this.targetArmy.squads[getRandom(0, (this.targetArmy.squads.length - 1) )]
    }
    getStrongesSquad() {
      let strongestSucces = 0;
      let strongest = this.targetArmy.squads[0];
      this.targetArmy.squads.map( (squad) => {
        if (squad.attackSuccess() > strongestSucces) {
          strongestSucces = squad.attackSuccess();
          strongest = squad
        }
      })
      return strongest
    }
    getWeakestSquad(){
      let weakestSucces = this.getStrongesSquad().attackSuccess();
      let weakest = this.targetArmy.squads[0];
      this.targetArmy.squads.map( (squad) => {
        if (squad.attackSuccess() < weakestSucces) {
          weakestSucces = squad.attackSuccess();
          weakest = squad
        }
      })
      return weakest
    }
}
