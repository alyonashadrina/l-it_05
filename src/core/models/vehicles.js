import {Unit} from "./unit";
import {Soldier} from "./soldier";
import {SoldierFabric} from "../../fabrics/soldier-fabric";
import {getGeomAvg} from "../../helpers/getGeomAvg";

let soldierFabric = SoldierFabric.getInstance();

export class Vehicle extends Unit {
    operators = [];

    constructor(health, recharge, operators) {
        super(health, recharge)
        this.operators = soldierFabric.createSoldiers(operators);
        // this.commonOperatorsSuccess = this.operators.map( operator => operator.attackSuccess());
    }

    makeDamage() {
      this.startRecharge();
      let commonOperatorsSuccess = 0;
      this.operators.map( operator => {
        commonOperatorsSuccess += operator.attackSuccess()
      });
      // console.log('Vehicle commonOperatorsSuccess',commonOperatorsSuccess, 0.1 + commonOperatorsSuccess / 100)
      return 0.1 + commonOperatorsSuccess / 100
    }

    attackSuccess() {
        let commonOperatorsSuccess = this.operators.map( operator => operator.attackSuccess())
        let success = 0.5 * (1 + this.health / 100) * getGeomAvg(commonOperatorsSuccess)
        return success
    }

    damageRecieve(damage) {
      this.setHealth( this.health - (60 * damage / 100) );
      this.operators.map( (operator, i) => {
        let opDamage = ( i == 0 ? 20 : 10 );
        operator.setHealth(operator.health - (opDamage * damage / 100) )
      })
    }

    setRecharge(val) {
        this.recharge = ( val > 1000 ? val : 1000 )
    }

    isAlive() {
      if (this.health > 0) {
        let operatorAlive = false;
        this.operators.map( operator => {
          if (operator.isAlive()) {
            operatorAlive = true
          }
        })
        return operatorAlive
      }
      return false
    }

}
