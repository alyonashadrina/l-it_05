import {BaseUnit} from "./base-unit";
import {getGeomAvg} from "../../helpers/getGeomAvg";

export class Squad extends BaseUnit {
    // _units = [];

    constructor(units, type) {
        super()
        this.units = units;
        this.type = type;
    }

    makeDamage() {
      let commonDamagePower = 0;
      this.units.map( unit => {
        // !unit.isRecharging &&
        if ( !unit.isRecharging && unit.isAlive() ) {
          return commonDamagePower += unit.makeDamage()
        }
      });
      // console.log('Squad commonDamagePower',commonDamagePower)

      return commonDamagePower
    }
    isAlive() {
      let unitsAlive = false;
      this.units.map( unit => {
        if (unit.isAlive()) {
          unitsAlive = true;
        }
      })
      return unitsAlive
    }
    attackSuccess() {
        // let commonSuccess = this.units.map( unit => {
        //   return unit.attackSuccess()
        // })
        let commonSuccess = 0;
        this.units.map( unit => {
          // !unit.isRecharging &&
          if ( !unit.isRecharging && unit.isAlive() ) {
            commonSuccess += unit.attackSuccess()
          }
        })
        // console.log('Squad attackSuccess',commonSuccess)

        return commonSuccess
    }
    damageRecieve(val) {
      if (this.type === "soldiers") {
        let soldiers = this.units;
        soldiers.map( soldier => {
          soldier.setHealth(soldier.health - val * 100);
        })
      } else {
        this.units.map( vehicle => vehicle.damageRecieve(val * 100) )
      }
    }

    incExperienceForUnits() {
        let soldiers;
        if (this.type === "soldiers") {
          soldiers = this.units
        } else {
          let soldiersNested = this.units.map( vehicle => {
            return vehicle.operators
          })
          soldiers = soldiersNested.reduce((a, b) => a.concat(b), []);
        }
        soldiers.map( soldier => {
          soldier.experience = soldier.experience + 1;

        })
    }

    startRechargeForUnits() {
        // loop through operators and recharge
    }


}
