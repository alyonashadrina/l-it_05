export class BaseUnit {
    get power() {
        return new Error('power() from BaseUnit need to be defined as sum of damage and life');
    }
    makeDamage() {
        throw new Error('makeDamage() from BaseUnit need to be defined');
    }
    isAlive() {
        throw new Error('isAlive() from BaseUnit need to be defined');
    }
    attackSuccess() {
        throw new Error('attackSuccess() from BaseUnit need to be defined');
    }
    damageRecieve() {
        throw new Error('damageRecieve() from BaseUnit need to be defined');
    }
}
