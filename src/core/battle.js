import {SoldierFabric} from "../fabrics/soldier-fabric";
import {SquadFabric} from "../fabrics/squad-fabric";
import {Strategy} from "./models/strategy"
import {getRandom} from "../helpers/getRandom";



export class Battle {
  armies = [];
  formedArmies = [];

  constructor(armies) {
    this.armies = armies;
  }

  formArmies(armies) {
    console.log(`----- START ARMIES FORMING -----`);
    this.formedArmies = this.armies.map( (item) => {
      console.log(`form ${item.name} Army`);
      let armySquadsFabric = SquadFabric.getInstance();
      let army = {
        squads: armySquadsFabric.createSquads(item.squads),
        strategy: item.strategy,
        name: item.name,
      }
      return army
    })
    console.log(`----- END ARMIES FORMING -----`);

  }

  startBattle() {
    let attackArmy = this.selectArmy();
    let armyToAttack = this.selectArmy(attackArmy);
    let attackArmySquad = this.selectSquad(attackArmy);
    let armyToAttackSquad = this.selectSquad(armyToAttack, attackArmy.strategy);

    let attackArmySquadSuccess = attackArmySquad.attackSuccess();
    let armyToAttackSquadSuccess = armyToAttackSquad.attackSuccess();

    // console.log( attackArmy.name, attackArmySquadSuccess )
    // console.log( armyToAttack.name, armyToAttackSquadSuccess )

    if ( attackArmySquadSuccess > armyToAttackSquadSuccess ) {
      let damage = attackArmySquad.makeDamage();
      armyToAttackSquad.damageRecieve(damage);
      attackArmySquad.incExperienceForUnits();

      let damageMessage = `DAMAGED`;
      if ( !armyToAttackSquad.isAlive() ) {
        for( let i = 0; i < armyToAttack.squads.length; i++ ){
           if ( armyToAttack.squads[i] === armyToAttackSquad) {
             armyToAttack.squads.splice(i, 1);
           }
        }
        damageMessage = `WAS TOTALY DESTROYED`
      }
      // console.log('attackArmySquad.makeDamage()', attackArmySquad.makeDamage())
      console.log(`${attackArmy.name} squad of ${attackArmySquad.units.length} ${attackArmySquad.type} attacks ${armyToAttack.name} squad of ${armyToAttackSquad.units.length} ${armyToAttackSquad.type}`)
      console.log(`  ${attackArmy.name} attack (success ${attackArmySquadSuccess.toFixed(2)}) was successful! ${armyToAttack.name} squad ${damageMessage} (success only ${armyToAttackSquadSuccess.toFixed(2)}).`)

      if ( armyToAttack.squads.length === 0 ) {
        for( let i = 0; i < this.formedArmies.length; i++ ){
           if ( this.formedArmies[i] === armyToAttack) {
             this.formedArmies.splice(i, 1);
           }
        }
        console.log(` ${armyToAttack.name} ${damageMessage}`)
        console.log(`---- Now there are only ${this.formedArmies.length} armies left`)

      }
    } else {
      // console.log(` ${attackArmy.name} attack didn't successed. There is no loser.`)
    }
  }

  selectArmy(army) {
    if (!army) {
      return this.formedArmies[getRandom(0, (this.formedArmies.length - 1) )]
    } else {
      let armyToAttack = this.formedArmies[getRandom(0, (this.formedArmies.length - 1) )];
      return (army.name == armyToAttack.name ? this.selectArmy(army) : armyToAttack)
    }
  }

  selectSquad(army, target) {
    const strategy = new Strategy(army);
    if (!target || target == 'random') {
      return strategy.getRandomSquad()
    } else if (target == 'weakest') {
      return strategy.getWeakestSquad()
    } else if (target == 'strongest') {
      return strategy.getStrongesSquad()
    }
  }

  startWar() {
    while (this.formedArmies.length > 1) {
        this.startBattle()
    }
    console.log('winer is ', this.formedArmies[0].name)
  }

  start() {
    this.formArmies(this.armies);
    this.startWar()
  }
}
